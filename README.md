### Background

This project includes trivial bash scripts intended to facilitate understanding of AWS rate limits.

### Approach

`tdi.sh` (test describe instances) runs test `di.sh`

`di.sh` takes three command line arguments

* instance_id
* total api calls to make
* target duration in seconds that all of the api calls are to fit in

`di.sh` invokes multiple instances of itself in parallel because the aws cli utility has fixed overhead

### Notes

* first round results were all against a single ec2 instance
* the test is setup such that a successful call sends a single dot (.) to stdout
  * an api call that does not return the instance id sends FAILURE to stdout and exits 1

### First Round Results

* the rate seemed to be throttle around 12 calls per second
* none of the calls failed.. they just returned no sooner than 0.083333 seconds

| total calls | target duration (s) | actual duration (s) | speed (calls per second) |
| ----------- | ------------------- | ------------------- | ------------------------ |
| 10          | 10                  | 10                  | 1                        |
| 20          | 10                  | 10                  | 2                        |
| 40          | 10                  | 10                  | 4                        |
| 80          | 10                  | 10                  | 8                        |
| 160         | 10                  | 13                  | 12                       |
| 320         | 10                  | 25                  | 13                       |
| 640         | 10                  | 54                  | 12                       |

