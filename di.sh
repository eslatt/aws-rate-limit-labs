#!/bin/bash

instance_id=$1
total_di_calls=$2
total_seconds=$3
sub_instance_id=$4
region=us-east-1
profile=default
# e.g. di i-01587809dbc73d22f 10 10 

if [ "$sub_instance_id" == "" ]
then
  echo " "
  echo " describe instance - SUP"
  echo "  instance_id=$instance_id"
  echo "  total_di_calls=$total_di_calls"
  echo "  total_seconds=$total_seconds"
  period_seconds=$(awk "BEGIN {print($total_seconds/$total_di_calls)}")
  min_cli_period_seconds=2
  total_required_clis=$(awk "BEGIN {print(int($min_cli_period_seconds/$period_seconds+0.5))}")
  echo "  total_required_clis=$total_required_clis"

  sub_total_calls=$(awk "BEGIN {print(int($total_di_calls/$total_required_clis+0.5))}")

  for (( c=1; c<=$total_required_clis; c++ ))
  do  
    ./di.sh $instance_id $sub_total_calls $total_seconds 1 &
    sleep $period_seconds
  done
  exit 0
fi


# echo testing describe-instances aws api rate limit
# echo " sub_instance_id=$sub_instance_id"

period_seconds=$(awk "BEGIN {print($total_seconds/$total_di_calls)}")
sleep_seconds=$(awk "BEGIN {print($period_seconds - 0.883333)}")

# echo period_seconds=$period_seconds

for (( c=1; c<=$total_di_calls; c++ ))
do  
  return_instance_id=wrong
  return_instance_id=$(aws ec2 describe-instances --region $region --profile $profile --instance-ids $instance_id --query "Reservations[].Instances[].InstanceId" --output text)
  if [ "$return_instance_id" != "$instance_id" ]; then echo "describe-instances sub instance $sub_instance_id call $c failed"; exit 1; fi
 # echo "describe-instances call $c succeeded"
  echo -n .
  sleep $sleep_seconds
done